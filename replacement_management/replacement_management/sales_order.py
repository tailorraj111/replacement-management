from __future__ import unicode_literals
import json
import frappe, erpnext
from frappe import _, scrub
from frappe.utils import cint, flt
from frappe.model.mapper import get_mapped_doc

@frappe.whitelist()
def make_sales_order_from_rma(source_name, target_doc=None, ignore_permissions=False):
	

	def set_missing_values(source, target):
		pass

	def update_item(obj, target, source_parent):
		target.qty = 1
		target.description = frappe.db.get_value("Item",obj.item_code,"description")
		target.uom = frappe.db.get_value("Item",obj.item_code,"stock_uom")
		target.income_account = "Sales - T"
		target.reference_dt = "Regular Replacement Request"
		target.reference_dn = source_parent.name

	doclist = get_mapped_doc("Regular Replacement Request", source_name, {
			"Regular Replacement Request": {
				"doctype": "Sales Order",
				"validation": {
					
				}
			},
			"RMA Items": {
				"doctype": "Sales Order Item",
				"field_map": {
					
				},
				"postprocess": update_item
			}
		}, target_doc, set_missing_values, ignore_permissions=ignore_permissions)

	# postprocess: fetch shipping address, set missing values

	return doclist