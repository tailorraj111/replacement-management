# Copyright (c) 2025, Raaj Tailor and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class CreditLimitForm(Document):
	def validate(self):
		# Query to calculate the closing balance
		result = frappe.db.sql("""
			SELECT SUM(debit) - SUM(credit) AS closing_balance
			FROM `tabGL Entry`
			WHERE party_type = 'Customer' AND party = %s
		""", (self.customer_name,), as_dict=True)

		# Update the closing_balance field in the document
		self.closing_balance = result[0]['closing_balance'] if result and result[0]['closing_balance'] is not None else 0

		# Calculate available_limit based on the given formula
		warner_electronics_approved_credit_limit = self.get("warner_electronics_approved_credit_limit", 0)
		channel_finance_company_approved_credit_limit = self.get("channel_finance_company_approved_credit_limit", 0)
		closing_balance = self.closing_balance

		# Calculate available limit
		available_limit = warner_electronics_approved_credit_limit + channel_finance_company_approved_credit_limit - closing_balance

		# Update the available_limit field in the document
		self.available_limit = available_limit