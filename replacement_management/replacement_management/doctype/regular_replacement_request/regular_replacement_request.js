// Copyright (c) 2021, Raaj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Regular Replacement Request', {
	refresh:async function(frm) {
		if(frm.doc.workflow_state == "Customer Approval"){
			frm.set_df_property("warranty_status","read_only",1)
			frm.set_df_property("approval_required","read_only",1)
			frm.set_df_property("repair_material_required","read_only",1)
		}

		if (frm.doc.workflow_state == "Draft"){
			cur_frm.add_custom_button(__('RMA Entry'), function() {
				frappe.set_route("rma-master", "new-rma-master");
				// frappe.set_route("rma-master");
				});
		}
		console.log($("[data-label='Send to Company']").parent().parent().parent().parent())
		if(frm.doc.workflow_state == "Draft"){
			// $("[data-label='Send to Company']").parent().hide()
			$("[data-label='Send to Company']").parent().parent().parent().parent().hide()
		}else{
			$("[data-label='Send to Company']").parent().parent().parent().parent().show()
		}
		if(frm.doc.workflow_state == "Ready to Dispatch" && frm.doc.approval_required == "Yes"){
			console.log("here")
			// $("[data-label='Send to Company']").parent().hide()
			$("[data-label='Dispatch']").parent().parent().parent().parent().hide()
		}else{
			$("[data-label='Dispatch']").parent().parent().parent().parent().show()
		}
		
		if((frm.doc.workflow_state == "Ready to Dispatch Hold" || frm.doc.workflow_state == "Ready to Dispatch") && frm.doc.approval_required == "Yes" && frm.doc.repair_material_required && frappe.user.has_role("RMA Admin")){
			cur_frm.add_custom_button(__('Create Sales Invoice'), function() {
				frappe.model.open_mapped_doc({
					method: "replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.get_map_doc",
					frm: cur_frm
					})
		
			})
		}
		if(frm.doc.workflow_state == "Dispatched"){
			cur_frm.add_custom_button(__('Stock Ledger'), function() {
				frappe.route_options = {
				"voucher_no": cur_frm.doc.name,
				"from_date": cur_frm.doc.date,
				"to_date": cur_frm.doc.date,
				"company": "Trueview",
				group_by_voucher: 0,
				group_by_account: 0,
				};
				frappe.set_route("query-report", "Stock Ledger");
			}, "View");
		}
		
		let loyalty_type_res = await frappe.db.get_value("Customer",frm.doc.customeradmin,"customer_loyalty");
        if(loyalty_type_res.message){
            var loyalty_type = loyalty_type_res.message.customer_loyalty;
            console.log(loyalty_type)
            let btn = document.createElement('span');
            if(loyalty_type == "New"){
                {
                    btn.innerText = 'New';
                    btn.style = 'color: blue;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }
            } else if(loyalty_type == "Silver") {
                {
                    btn.innerText = 'Silver';
                    btn.style = 'color: green;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }
            } else if(loyalty_type == "Gold") {
                {
                    btn.innerText = 'Gold';
                    btn.style = 'color: orange;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }

            } else if(loyalty_type == "Platinum")
                {
                    btn.innerText = 'Platinum';
                    btn.style = 'color: red;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }
                var tool_bar = $("div[data-page-route='Regular Replacement Request'] > .page-head > .container > .page-head-content > .page-title")
                console.log($("div[data-page-route='Regular Replacement Request'] > .page-head > .container > .page-head-content > .page-title"))
                console.log($("#loayalty_indicator"))
                $( "#loayalty_indicator" ).remove();
                // if(!tool_bar.find("#loayalty_indicator")){
                    tool_bar.append(btn)
                // }
                
                console.log($(".form-inner-toolbar"))
		}
	},
	value_changes:function(frm){
		
		frm.set_df_property("warranty_status","read_only",0)
		frm.set_df_property("approval_required","read_only",0)
		frm.set_df_property("repair_material_required","read_only",0)
	
		frm.set_value("value_changed",1)
		frm.refresh_field("warranty_status","approval_required","repair_material_required","value_changed")
	

	},
	workflow_state:function(frm){
		if(frm.doc.workflow_state == "Draft"){
			$("[data-label='Send to Company']").parent().hide()
		}
	},
	before_save:async function(frm){
		if(!frm.doc.terms_check)
		{

			var terms_details = await frappe.db.get_value("Terms and Conditions","RR Terms","terms")
			if(terms_details.message.terms){
				return new Promise(function(resolve, reject) {
					frappe.confirm(
						terms_details.message.terms,
						function() {
							var negative = 'frappe.validated = false';
							resolve(negative);
							frm.set_value("terms_check",1)
							frm.set_value("agree_to_terms_and_conditions",1)
							frm.refresh_field("terms_check","agree_to_terms_and_conditions")
						},
						function() {
							reject();
						}
					)
				})

			}
		}
		// console.log(terms_details.message.terms)
		

	},
	setup: function(frm) {
	    console.log(frappe.session.user)
		frm.set_query("distributor_name", function() {
			return {
				filters: {
				    "user_name":frappe.session.user
				},
				query:"replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.filter_distributor"
			}
		});
		frm.set_query("rma_engineer", function() {
			return {
				query:"replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.get_rma_engineer"
			}
		});
	},
	onload: function(frm) {
		console.log(frappe.session.user)
		if(frm.doc.__islocal){
			frappe.call({
				"method":"replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.get_customer_details",
				"args":{"uname":frappe.session.user},
				"callback":function(res){
					console.log(res)
					if( res.message[0]){
						frm.set_value("customer_name",res.message[0])
						frm.refresh_field("customer_name")
					}
					if( res.message[1]){
						frm.set_value("customer_mobile_no",res.message[1])
						frm.refresh_field("customer_mobile_no")
					}
	
				}
	
			})

		}

		// Get Sales invoice automatic 
        if (frm.doc.workflow_state === 'Ready to Dispatch Hold' && !frm.doc.sales_invoice_ref) {
            frappe.call({
                method: 'frappe.client.get_list',
                args: {
                    doctype: 'Sales Invoice',
                    filters: { rr_reference: frm.doc.name },
                    fields: ['name']
                },
                callback: function (r) {
                    if (r.message && r.message.length > 0) {
                        frm.set_value('sales_invoice_ref', r.message[0].name);
                        frm.save();
                    }
                }
            });
        }
	},

	validate:function(frm){
		// var total= 0

		// if(frm.doc.repair_material_required.length > 0){
		// 	frm.doc.repair_material_required.forEach(element => {
		// 		total = element.rate_including_tax + total
		// 	});
		// 	frm.set_value("total_amount",total)
		// 	frm.refresh_field("total_amount")

		// }
		
	}
});

// cur_frm.fields_dict['repair_material_required'].grid.get_field('item_code').get_query = function(doc) {
// 	return {
// 		 filters: {
// 		     "item_group": ["=", 'Raw Material']
// 		 }
		 
	
		 
		 
// 	}
// }


frappe.ui.form.on('RMA Items', {
	rate:function(frm,cdt,cdn){
		var row = locals[cdt][cdn]
		if(row.rate){
			console.log(row.rate)
			var wo_gst = row.rate*1.18
			row.rate_including_tax = wo_gst
			frm.refresh_field("repair_material_required")
		}
	}
})