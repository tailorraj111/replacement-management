# -*- coding: utf-8 -*-
# Copyright (c) 2021, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.contacts.doctype.contact.contact import get_contact_name
from erpnext.controllers.stock_controller import StockController
from frappe.utils import now
import json
from erpnext.accounts.party import get_party_details

@frappe.whitelist()
def filter_distributor(doctype, txt, searchfield, start, page_len, filters):
	dr = frappe.db.get_value("Dealer Registration",{"user_id":filters.get("user_name")},"name")
	response = frappe.db.get_list("Distributor Details",{"parent":dr},["distributor"],as_list=1)
	return response

class RegularReplacementRequest(StockController,Document):

	def validate(self):
		if len(self.repair_material_required) > 0:
			total = 0
			for item in self.repair_material_required:
				if item.rate_including_tax:
					total=total + item.rate_including_tax
			
			if total:
				self.total_amount = total
		else:
			self.total_amount = 0

		if self.workflow_state == "Dispatched" and self.converted_from_p2p:
			self.create_p2p_stock_transfer()

		if self.workflow_state == "Ready to Dispatch" and self.approval_required == "No" and not self.stock_deducted:
			if self.repair_material_required:
				self.create_replacement_stock_entry()
				self.stock_deducted = 1
			else:
				frappe.msgprint("Please Set Data in Repair Material Required Table")

			
		frappe.db.set_value('Issue', self.issue_number, 'rr_number', self.name)

		status_log(self)

	def create_p2p_stock_transfer(self):
		p2p_fresh = frappe.db.get_single_value('Regular Replacement Settings', 'fresh_warehouse')
		if not p2p_fresh:
			frappe.throw("Please Set Value of Fresh Warehouse In Regular Replacement Settings!")

		p2p_faulty = frappe.db.get_single_value('Regular Replacement Settings', 'faulty_warehouse')
		if not p2p_faulty:
			frappe.throw("Please Set Value of Faulty Warehouse In Regular Replacement Settings!")

		department = frappe.db.get_single_value('Regular Replacement Settings', 'department')
		if not department:
			frappe.throw("Please Set Value of Department In Regular Replacement Settings!")
			
		division = frappe.db.get_single_value('Regular Replacement Settings', 'division')
		if not division:
			frappe.throw("Please Set Value of Division In Regular Replacement Settings!")

		try:
			se = frappe.new_doc('Stock Entry')
			se.stock_entry_type = 'Material Transfer'
			se.stock_entry_for = 'Material Transfer'
			se.posting_date = frappe.utils.now()
			se.from_warehouse = p2p_faulty
			se.to_warehouse = p2p_fresh
			se.remarks = self.name
			se.department = "Replacement Department - T"
			se.division = "CCTV Division"
			se.append('items', {
				'item_code': self.item_code,
				's_warehouse': p2p_faulty,
				't_warehouse': p2p_fresh,
				'qty': 1
			})
			se.save()
			se.submit()
		except Exception as e:
			frappe.log_error(title="Stock Entry creation failed from regular replacement request", message=str(e))
			frappe.throw("Stock Entry Creation Failed From Regular Replacement Request,Kindly Check Logs")

	def create_replacement_stock_entry(self):
		try:
			difference_account = frappe.db.get_single_value('Regular Replacement Settings', 'replacement_expense_account')
			if not difference_account:
				frappe.throw("Please Set Value of Replacement Expense Account In Regular Replacement Settings!")
			
			source_warehouse = frappe.db.get_single_value('Regular Replacement Settings', 'source_warehouse')
			if not difference_account:
				frappe.throw("Please Set Value of Source Warehouse In Regular Replacement Settings!")

			stock_entry_doc = frappe.new_doc("Stock Entry")
			stock_entry_doc.stock_entry_type = "Material Issue"
			stock_entry_doc.posting_date = self.date
			stock_entry_doc.custom_regular_replacement_request = self.name

			for item in self.repair_material_required:
				if item.is_serialize == 1 and item.serial_no:
					stock_entry_doc.append("items", {
						"item_code": item.item_code,
						"qty": 1,
						"s_warehouse": source_warehouse,
						"expense_account":difference_account,
						"use_serial_batch_fields":"1",
						"serial_no":item.serial_no
					})
				else:
					stock_entry_doc.append("items", {
						"item_code": item.item_code,
						"qty": 1,
						"s_warehouse": source_warehouse,
						"expense_account":difference_account
					})


			stock_entry_doc.save()
			stock_entry_doc.submit()
		except Exception as e:
			frappe.log_error(title="Stock Entry creation failed from regular replacement request", message=str(e))
			frappe.throw("Stock Entry Creation Failed From Regular Replacement Request,Kindly Check Logs")	

@frappe.whitelist()
def status_log(self):
	
	number_status_log = len(self.get("status_log"))
	if(int(number_status_log) == 0):
		self.append("status_log",{
			"status": self.workflow_state,
			"created_by": frappe.session.user,
			"date": now(),
		})
	else:
		item = self.status_log[-1]
		if(self.workflow_state != item.status):
			self.append("status_log",{
				"status": self.workflow_state,
				"created_by": frappe.session.user,
				"date": now(),
			})


@frappe.whitelist()
def get_customer_details(uname):
	party = None
	contact_name = get_contact_name(uname)
	if contact_name:
		# frappe.msgprint(str(contact_name))
		contact = frappe.get_doc('Contact', contact_name)

		if contact.links:
			party_doctype = contact.links[0].link_doctype
			party = contact.links[0].link_name
	if party:
		# frappe.msgprint(str(party))
		cust_doc = frappe.get_doc(party_doctype, party)
		# return (cust_doc.customer_name,cust_doc.mobile_no)
		return (cust_doc.lead_name,cust_doc.mobile_no)

@frappe.whitelist()
def get_map_doc(source_name, target_doc=None):
 
    from frappe.model.mapper import get_mapped_doc
 
    def set_missing_values(source, target):
        company = frappe.db.get_value("Global Defaults",None,"default_company")
        party_details = get_party_details(party=source.customeradmin,party_type='Customer',posting_date=frappe.utils.today(),company=company,doctype='Sales Order')
        target.taxes_and_charges = party_details.get("taxes_and_charges")
        target.set("taxes", party_details.get("taxes"))        
        target.set_missing_values()
        target.calculate_taxes_and_totals()
    
    def update_item(obj, target, source_parent):
        target.qty = 1
        target.description = obj.item_name
        target.uom = frappe.db.get_value("Item",obj.item_code,"stock_uom")
        target.income_account = "Sales - T"
        target.reference_dt = "Regular Replacement Request"
        target.reference_dn = source_parent.name
        tax_category = frappe.db.get_value("Customer",source_parent.customeradmin,"tax_category")
        target.item_tax_template = frappe.db.get_value("Item Tax",{"parent":obj.item_code,"tax_category":tax_category},"item_tax_template")
        target.run_method("get_item_tax_info")
		
            
    doc = get_mapped_doc("Regular Replacement Request", source_name,   {
        "Regular Replacement Request": {
            "doctype": "Sales Order",
            "field_map": {
                "name":"rr_reference",
				"customeradmin":"customer"
            }
        },
		"RMA Items":{
			"doctype":"Sales Order Item",
			"field_map":{
				
			},
			"postprocess": update_item
		}
 
    }, target_doc, set_missing_values)
    doc.ref_voucher = "Regular Replacement Request"
    
 
    return doc


@frappe.whitelist()
def get_permission_query_conditions(user):
	
	if "RMA Admin" in frappe.get_roles(user):
		return None
	elif "RMA Engineer" in frappe.get_roles(user):
		return None
	
	elif "Sales Executive" in frappe.get_roles(user):
		return """`tabRegular Replacement Request`.`customeradmin` in (select customer_name from `tabCustomer` where account_manager = "{user}")""".format(user=frappe.session.user)
	else:
		return "(`tabRegular Replacement Request`.owner = {user}) or (`tabRegular Replacement Request`.customer_emailadmin = {user}) or (`tabRegular Replacement Request`.distributor_email = {user})".format(user = frappe.db.escape(user))
		

@frappe.whitelist()
def get_rma_engineer(doctype, txt, searchfield, start, page_len, filters):
	return frappe.desk.reportview.execute("User", filters = [["Has Role","role","=","RMA Engineer"]],fields = ["name"],limit_start=0, limit_page_length=100, order_by = "name", as_list=True)