# -*- coding: utf-8 -*-
# Copyright (c) 2021, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import getdate
from frappe.contacts.doctype.contact.contact import get_contact_name

class DOARequest(Document):
	@frappe.whitelist()
	def get_customer_details(self,uname):
		party = None
		contact_name = get_contact_name(uname)
		if contact_name:
			# frappe.msgprint(str(contact_name))
			contact = frappe.get_doc('Contact', contact_name)

			if contact.links:
				party_doctype = contact.links[0].link_doctype
				party = contact.links[0].link_name
		if party:
			# frappe.msgprint(str(party))
			cust_doc = frappe.get_doc(party_doctype, party)
			if cust_doc.doctype == "Lead":
				return (cust_doc.lead_name,cust_doc.mobile_no)
			else:
				return (cust_doc.customer_name,cust_doc.mobile_no)

	def validate(self):
		if self.batch_no:
			# frappe.msgprint("batch no "+str(self.batch_no))
			# valid_upto = frappe.db.get_value("Batch Number Validity",self.batch_no,"valid_upto")
			valid_upto = frappe.db.get_value("Batch Number Validity",str(self.batch_no)[-2:],"valid_upto")
			if not valid_upto:
				frappe.throw("Please Enter Valid Batch Number!")
			if valid_upto and getdate(self.date) > valid_upto:
				frappe.throw("This Product Is Out of Warranty, Please Process through Regular Replacement!")

@frappe.whitelist()
def get_map_doc(source_name, target_doc=None):
 
    from frappe.model.mapper import get_mapped_doc
 
    def set_missing_values(source, target):
        pass
        # target.tax_category = frappe.db.get_value("Customer",source.customeradmin,"tax_category")
        # target.run_method("calculate_taxes_and_totals")
        
            
    doc = get_mapped_doc("DOA Request", source_name,   {
        "DOA Request": {
            "doctype": "Sales Order",
            "field_map": {
				"customer_name":"customer",
                "name":"doa_reference",
                "customeradmin":"customer"
            }
        }
 
    }, target_doc, set_missing_values)
    doc.ref_voucher = "DOA Request"
    
 
    return doc
	

@frappe.whitelist()
def get_permission_query_conditions(user):
	if any(role in frappe.get_roles(user) for role in ["RMA Admin", "RMA Engineer", "System Manager", "Credit Control"]):
		return None
	elif "Dealer" in frappe.get_roles(user):
		user_escaped = frappe.db.escape(user)
		query = f"""
			(`tabDOA Request`.owner = {user_escaped})
			OR
			(`tabDOA Request`.customer_name IN 
				(SELECT cs.name FROM tabCustomer AS cs WHERE cs.email_id = {user_escaped}))
		"""
		return query
	else:
		return None
