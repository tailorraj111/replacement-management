// Copyright (c) 2021, Raaj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('DOA Request', {
	refresh:async function(frm) {
		// if (frm.doc.workflow_state == "DOA Issued" || frm.doc.workflow_state == "DOA Approved"){
		if (frm.doc.workflow_state == "DOA Issued"){
			$("[data-label='Send To Unit']").parent().hide()
		}
		if(frm.doc.workflow_state == "DOA Received"){
			cur_frm.add_custom_button(__('Create Sales'), function() {
				frappe.model.open_mapped_doc({
					method: "replacement_management.replacement_management.doctype.doa_request.doa_request.get_map_doc",
					frm: cur_frm
					})
		
			})
		}
		if (frm.doc.workflow_state == "DOA Approved"){
			cur_frm.add_custom_button(__('RMA Entry'), function() {
				frappe.set_route("rma-master", "new-rma-master");
				// frappe.set_route("rma-master");
				});
		}

		let loyalty_type_res = await frappe.db.get_value("Customer",frm.doc.customeradmin,"customer_loyalty");
        if(loyalty_type_res.message){
            var loyalty_type = loyalty_type_res.message.customer_loyalty;
            console.log(loyalty_type)
            let btn = document.createElement('span');
            if(loyalty_type == "New"){
                {
                    btn.innerText = 'New';
                    btn.style = 'color: blue;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }
            } else if(loyalty_type == "Silver") {
                {
                    btn.innerText = 'Silver';
                    btn.style = 'color: green;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }
            } else if(loyalty_type == "Gold") {
                {
                    btn.innerText = 'Gold';
                    btn.style = 'color: orange;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }

            } else if(loyalty_type == "Platinum")
                {
                    btn.innerText = 'Platinum';
                    btn.style = 'color: red;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }
                var tool_bar = $("div[data-page-route='DOA Request'] > .page-head > .container > .page-head-content > .page-title")
                console.log($("div[data-page-route='DOA Request'] > .page-head > .container > .page-head-content > .page-title"))
                console.log($("#loayalty_indicator"))
                $( "#loayalty_indicator" ).remove();
                // if(!tool_bar.find("#loayalty_indicator")){
                    tool_bar.append(btn)
                // }
                
                console.log($(".form-inner-toolbar"))
		}
	},
	workflow_state:function(frm){
		if (frm.doc.workflow_state == "DOA Issued" || frm.doc.workflow_state == "DOA Approved"){
			$("[data-label='Send To Unit']").parent().hide()
		}
	},
	before_save:async function(frm){
		if(!frm.doc.terms_check)
		{
			var terms_details = await frappe.db.get_value("Terms and Conditions","DOA Terms","terms")
			if(terms_details.message.terms){
				return new Promise(function(resolve, reject) {
					frappe.confirm(
						terms_details.message.terms,
						function() {
							var negative = 'frappe.validated = false';
							resolve(negative);
							frm.set_value("terms_check",1)
							frm.set_value("agree_to_terms_and_conditions",1)
							frm.refresh_field("terms_check","agree_to_terms_and_conditions")
						},
						function() {
							reject();
						}
					)
				})

			}
		}	

	},
	onload(frm){
		if(frm.is_new()){
			frappe.call({
				"method":"get_customer_details",
				"doc":frm.doc,
				"args":{"uname":frappe.session.user},
				"callback":function(res){
					console.log(res)
					if( res.message[0]){
						frm.set_value("customer_name",res.message[0])
						frm.refresh_field("customer_name")
					}
					if( res.message[1]){
						frm.set_value("customer_mobile_no",res.message[1])
						frm.refresh_field("customer_mobile_no")
					}
	
				}
	
			})

		}
	}
});
