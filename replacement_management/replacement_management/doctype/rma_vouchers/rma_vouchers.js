// Copyright (c) 2022, Raaj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('RMA Vouchers', {
	refresh(frm) {
		frm.add_custom_button('Get Vouchers',function(){
	       // frappe.msgprint("get your voucher here");
	       if(frm.doc.customer){
	           frappe.db.get_list('Regular Replacement Request', {
                    fields: ['name','item_code', 'item_name','issue','total_amount'],
                    filters: {
                        workflow_state: 'Customer Approval',
                        customer_name: frm.doc.customer
                    }
                }).then(records => {
                    console.log(records);
                    cur_frm.clear_table("rma_voucher");
                    cur_frm.refresh_fields("rma_voucher");
                    for (var i in records){
                        console.log(records[i].item_code);
                        var newrow = frappe.model.add_child(frm.doc, "RMA Voucher Table", "rma_voucher");
                        newrow.rr_voucher_name = records[i].name;
                        newrow.problem_detected = records[i].issue;
                        newrow.item_code = records[i].item_code;
                        newrow.item_name = records[i].item_name;
                        newrow.approval_amount = records[i].total_amount;
                    }
                     refresh_field('rma_voucher');
                });
	       }else{
	            cur_frm.clear_table("rma_voucher");
                cur_frm.refresh_fields("rma_voucher");
	       }
	       
	});
	    frm.add_custom_button('Send Email',function(){
	       // frappe.msgprint("get your voucher here");
	       
	       frm.email_doc("Hello {{customer_email}}");

	});
	
	},
	
})
