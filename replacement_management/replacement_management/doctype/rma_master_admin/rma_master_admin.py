# -*- coding: utf-8 -*-
# Copyright (c) 2021, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class RMAMasterAdmin(Document):
	def validate(self):
		rma_list = []
		for item in self.regular_replacement:
			if item.rr_voucher in rma_list:
				frappe.throw("Regular Replace No {} is Repeating".format(item.rr_voucher))
			else:
				rma_list.append(item.rr_voucher)
