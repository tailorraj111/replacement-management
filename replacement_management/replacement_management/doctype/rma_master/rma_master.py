# -*- coding: utf-8 -*-
# Copyright (c) 2020, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.contacts.doctype.contact.contact import get_contact_name

class RMAMaster(Document):
	def on_submit(self):
		if len(self.rma_list) > 0 and (not self.courier_name or not self.courier_receipt_no or not self.number_of_box or not self.mode_of_transport or not self.gst_vehicle_type):
			frappe.throw("Please fill all Courier Details!")
	
	@frappe.whitelist()
	def get_customer_details(self,uname):
		party = None
		contact_name = get_contact_name(uname)
		if contact_name:
			# frappe.msgprint(str(contact_name))
			contact = frappe.get_doc('Contact', contact_name)

			if contact.links:
				party_doctype = contact.links[0].link_doctype
				party = contact.links[0].link_name
		if party:
			# frappe.msgprint(str(party))
			cust_doc = frappe.get_doc(party_doctype, party)
			if cust_doc.doctype == "Lead":
				return (cust_doc.lead_name,cust_doc.mobile_no)
			else:
				return (cust_doc.customer_name,cust_doc.mobile_no)
	def validate(self):
		# pass
		rma_list = []
		for item in self.rr_list:
			if item.rr_voucher in rma_list:
				frappe.throw("Regular Replace No {} is Repeating".format(item.rr_voucher))
			else:
				rma_list.append(item.rr_voucher)
	
	@frappe.whitelist()
	def get_company_add(self):
		add_doc =  frappe.get_doc("Address",{"is_your_company_address":1})
		add_str = add_doc.address_line1 +","+ add_doc.address_line2 if add_doc.address_line2 else "" +","+ add_doc.city +","+ add_doc.state
		return add_str

@frappe.whitelist()
def get_permission_query_conditions(user):
	# customer_name = frappe.db.get_value("Dealer Registration",{"user_id":frappe.session.user},"registered_name")
	
	if "RMA Admin" in frappe.get_roles(user):
		return None
	elif "RMA Engineer" in frappe.get_roles(user):
		return None
	# elif "Sales Executive" in frappe.get_roles(user):
	# 	return None
		# return """\
		#  (`tabRegular Replacement Request`.name in (select tabToDo.reference_name from tabToDo where
		#  	(tabToDo.owner = {user} or tabToDo.assigned_by = {user}) 
		#  	and tabToDo.reference_type = 'Regular Replacement Request' and tabToDo.reference_name=`tabRegular Replacement Request`.name and tabToDo.status = 'Open'))\
		#  """.format(user=frappe.db.escape(user))
	else:
		return "(`tabRMA Master`.owner = {user}) or (`tabRMA Master`.customer in (select cs.name from tabCustomer as cs where cs.email_id = '{user}'))".format(user = frappe.db.escape(user))
		# return """(tabRMA.customer_name = {customer})""".format(customer=customer_name)



