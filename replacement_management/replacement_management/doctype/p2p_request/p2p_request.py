# -*- coding: utf-8 -*-
# Copyright (c) 2021, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.contacts.doctype.contact.contact import get_contact_name
from erpnext.controllers.accounts_controller import AccountsController
from erpnext.controllers.stock_controller import StockController
from erpnext.accounts.utils import get_fiscal_year, validate_fiscal_year, get_account_currency
from frappe.utils import getdate
from frappe.desk.reportview import get_filters_cond, get_match_cond

class P2PRequest(StockController,AccountsController,Document):
	def validate(self):
		if self.batch_no:
			# frappe.msgprint("batch no "+str(self.batch_no)[-2:])
			valid_upto = frappe.db.get_value("Batch Number Validity",str(self.batch_no)[-2:],"valid_upto")
			if not valid_upto:
				frappe.throw("Please Enter Valid Batch Number!")
			if valid_upto and getdate(self.date) > valid_upto:
				frappe.throw("This Product Is Out of Warranty, Please Process through Regular Replacement!")
		# frappe.msgprint("here")
		customer = self.get_customer_details(self.distributor_email)

		if self.workflow_state == "P2P Issued":
			p2p_fresh = frappe.db.get_single_value('Regular Replacement Settings', 'fresh_warehouse')
			p2p_faulty = frappe.db.get_single_value('Regular Replacement Settings', 'faulty_warehouse')
			se = frappe.new_doc('Stock Entry')
			se.stock_entry_type = 'Material Issue'
			se.stock_entry_for = 'Material Issue'
			se.posting_date = frappe.utils.now()
			se.from_warehouse = p2p_fresh
			se.remarks = self.name
			se.department = "Replacement Department - T"
			se.division = "CCTV Division"
			se.append('items', {
				'item_code': self.replaced_item_code,
				's_warehouse': p2p_fresh,
				'qty': 1
			})
			se.save()
			se.submit()

		if self.workflow_state == "P2P Received":
			p2p_fresh = frappe.db.get_single_value('Regular Replacement Settings', 'fresh_warehouse')
			p2p_faulty = frappe.db.get_single_value('Regular Replacement Settings', 'faulty_warehouse')
			se = frappe.new_doc('Stock Entry')
			se.stock_entry_type = 'Material Receipt'
			se.stock_entry_for = 'Material Receipt'
			se.posting_date = frappe.utils.now()
			se.to_warehouse = p2p_faulty
			se.remarks = self.name
			se.department = "Replacement Department - T"
			se.division = "CCTV Division"
			se.append('items', {
				'item_code': self.item_code,
				't_warehouse': p2p_faulty,
				'qty': 1
			})
			se.save()
			se.submit()

		# frappe.msgprint(str(customer[0]))
		# faulty,fresh,transit = frappe.db.get_value("Customer",customer[0],['faulty_warehouse','fresh_warehouse','in_transit_warehouse'])
		# if self.workflow_state == "P2P Issued":
		# 	self.deduct_stock_from_warehouse({
		# 		"add_item":self.item_code,
		# 		"add_warehouse":faulty,
		# 		"deduct_item":self.replaced_item_code,
		# 		"deduct_warehouse":fresh
		# 	})
		# if self.workflow_state == "Sent to Unit":
		# 	self.deduct_stock_from_warehouse({
		# 		"add_item":self.item_code,
		# 		"add_warehouse":transit,
		# 		"deduct_item":self.item_code,
		# 		"deduct_warehouse":faulty
		# 	})
		# if self.workflow_state == "P2P Received":
			# self.deduct_stock_from_warehouse({
			# 	"add_item":self.item_code,
			# 	"add_warehouse":"P2P Faulty - T",
			# 	"deduct_item":self.item_code,
			# 	"deduct_warehouse":transit
			# })
			# frappe.throw("stop")
		p2p_master_check = frappe.db.get_value("P2P Master",{"item_code":self.item_code},"status")
		if not p2p_master_check:
			frappe.throw("Item is not available under P2P replacement, Please proceed with regular replacement")
		else:
			if p2p_master_check != "Enable":
				frappe.throw("Item is not available under P2P replacement, Please proceed with regular replacement")


		if self.photo_1 or self.photo_2 or self.photo_3 or self.photo_4 or self.photo_5:
			pass
		else:
			frappe.throw("Attach at least 1 Image!!!")

		if self.workflow_state == "P2P Issued" and (not self.replaced_item_code and not self.replaced_item_batch_no and not self.replaced_item_serial_no):
			frappe.throw("Please Enter Replaced Item Details!")
		
	@frappe.whitelist()
	def make_rr(self):
		doc = frappe.get_doc({
			'doctype': 'Regular Replacement Request',
			'customeradmin': self.customeradmin,
			'customer_rma_no': self.customer_rma_no,
			'your_customer_name': self.dealer_name,
			'invoice_no': self.invoice_no,
			'invoice_date': self.invoice_date,
			'item_code': self.item_code,
			'item_name': self.item_name,
			'issue': self.issue,
			'batch_no': self.batch_no,
			'serial_number': self.serial_number,
			'photo_1': self.photo_1,
			'photo_2': self.photo_2,
			'photo_3': self.photo_3,
			'photo_4': self.photo_4,
			'photo_5': self.photo_5,
			'comment_for_dealer': self.comment_for_dealer,
			'comment_by_dealer': self.comment_by_dealer,
			'converted_from_p2p': 1
		})
		doc.insert()
		frappe.db.set_value("P2P Request",self.name,"rr_reference",doc.name)
		return doc.name

		
	@frappe.whitelist()
	def get_customer_details(self,uname):
		party = None
		contact_name = get_contact_name(uname)
		if contact_name:
			# frappe.msgprint(str(contact_name))
			contact = frappe.get_doc('Contact', contact_name)

			if contact.links:
				party_doctype = contact.links[0].link_doctype
				party = contact.links[0].link_name
		if party:
			# frappe.msgprint(str(party))
			cust_doc = frappe.get_doc(party_doctype, party)
			# return (cust_doc.customer_name,cust_doc.mobile_no)
			return (cust_doc.lead_name,cust_doc.mobile_no)

	# def deduct_stock_from_warehouse(self,args):
		
	# 	sl_entries = []
	# 	sl_entries.append(frappe._dict({
	# 	"item_code": args['deduct_item'],
	# 	"warehouse": args['deduct_warehouse'],
	# 	"posting_date": self.date,				
	# 	"voucher_type": self.doctype,
	# 	"voucher_no": self.name,
	# 	"actual_qty": -1,				
	# 	"company": "Trueview",				
	# 	"is_cancelled": self.docstatus==2 and "Yes" or "No"
	# 	}))
	# 	sl_entries.append(frappe._dict({
	# 	"item_code": args['add_item'],
	# 	"warehouse": args['add_warehouse'],
	# 	"posting_date": self.date,				
	# 	"voucher_type": self.doctype,
	# 	"voucher_no": self.name,
	# 	"actual_qty": 1,				
	# 	"company": "Trueview",				
	# 	"is_cancelled": self.docstatus==2 and "Yes" or "No"
	# 	}))
	# 	# frappe.throw(str(sl_entries))
	# 	self.make_sl_entries(sl_entries, self.docstatus==2 and 'Yes' or 'No')	


@frappe.whitelist()
def get_permission_query_conditions(user):
	# customer_name = frappe.db.get_value("Dealer Registration",{"user_id":frappe.session.user},"registered_name")
	
	if "RMA Admin" in frappe.get_roles(user):
		return None
	if "RMA Engineer" in frappe.get_roles(user):
		return None
	else:
		return "(`tabP2P Request`.owner = {user}) or (`tabP2P Request`.customer_emailadmin = {user}) or (`tabP2P Request`.distributor_email = {user})".format(user = frappe.db.escape(user))
		# return """(tabRMA.customer_name = {customer})""".format(customer=customer_name)

@frappe.whitelist()
def filter_item(doctype, txt, searchfield, start, page_len, filters):
	# response = frappe.db.get_all("P2P Master",{"status":"Enable"},["item_code"],as_list=1)
	response = frappe.db.sql("""
	select item_code from `tabP2P Master` where status = "Enable"
	AND {key} LIKE %(txt)s
                
            {mcond}
        ORDER BY
            item_code
        
    """.format(**{
            'key': searchfield,
            'mcond':get_match_cond(doctype)
        }), {
        'txt': "%{}%".format(txt),
        '_txt': txt.replace("%", ""),
        'start': start,
        'page_len': page_len
    })
	# frappe.msgprint(str(response))
	
	return response