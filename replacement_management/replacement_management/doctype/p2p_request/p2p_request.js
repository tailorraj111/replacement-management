// Copyright (c) 2021, Raaj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('P2P Request', {
	onload:function(frm){
		
		
	},
	refresh:async function(frm){
		if(frm.doc.replaced_item_code){
			frm.remove_custom_button('Create Replacement')
		}
		if (frm.doc.workflow_state == "Closed" && !frm.doc.replaced_item_code ){
			
			cur_frm.add_custom_button(__('Create Replacement'), function() {
				if(frm.doc.rr_reference){
					frappe.throw("Your Regular Replacement Form Is Already Created: " +frm.doc.rr_reference)
				}
				frappe.call({
					"method":"make_rr",
					"doc":frm.doc,
					"callback":function(res){
						console.log(res)
						if(res.message){
							frappe.msgprint("Regular Replacement " + res.message + " Created Successfully!")
						}	
					}
		
				})
			});
		}

		if (frm.doc.workflow_state == "P2P Issued" || frm.doc.workflow_state == "P2P Approved"){
			$("[data-label='Send To Unit']").parent().hide()
		}
		
		console.log(frm.doc.workflow_state)
		if(frm.doc.workflow_state == "Sent to Company"){
			frm.set_df_property("replaced_item_code","reqd",1)
			frm.set_df_property("replaced_item_batch_no","reqd",1)
			frm.set_df_property("replaced_item_serial_no","reqd",1)
			
		}


		// if(frm.doc.workflow_state == "P2P Issued"){

			// cur_frm.add_custom_button(__('Accounts Ledger'), function() {
			// frappe.route_options = {
			// "voucher_no": cur_frm.doc.name,
			// "from_date": cur_frm.doc.date,
			// "to_date": cur_frm.doc.date,
			// "company": cur_frm.doc.company,
			// group_by_voucher: 0,
			// group_by_account: 0,
			// };
			// frappe.set_route("query-report", "General Ledger");
			// }, "View");


			if (frm.doc.workflow_state == "P2P Approved"){
				cur_frm.add_custom_button(__('RMA Entry'), function() {
					frappe.set_route("rma-master", "new-rma-master");
					// frappe.set_route("rma-master");
					});
			}
			
			cur_frm.add_custom_button(__('Stock Ledger'), function() {
			frappe.route_options = {
			"voucher_no": cur_frm.doc.name,
			"from_date": cur_frm.doc.date,
			"to_date": cur_frm.doc.date,
			"company": "Trueview",
			group_by_voucher: 0,
			group_by_account: 0,
			};
			frappe.set_route("query-report", "Stock Ledger");
			}, "View");
		
			
		// }

		let loyalty_type_res = await frappe.db.get_value("Customer",frm.doc.customeradmin,"customer_loyalty");
        if(loyalty_type_res.message){
            var loyalty_type = loyalty_type_res.message.customer_loyalty;
            console.log(loyalty_type)
            let btn = document.createElement('span');
            if(loyalty_type == "New"){
                {
                    btn.innerText = 'New';
                    btn.style = 'color: blue;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }
            } else if(loyalty_type == "Silver") {
                {
                    btn.innerText = 'Silver';
                    btn.style = 'color: green;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }
            } else if(loyalty_type == "Gold") {
                {
                    btn.innerText = 'Gold';
                    btn.style = 'color: orange;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }

            } else if(loyalty_type == "Platinum")
                {
                    btn.innerText = 'Platinum';
                    btn.style = 'color: red;font-size: 28px;float: left;font-weight: bold;';
                    btn.id = "loayalty_indicator"
                }
                var tool_bar = $("div[data-page-route='P2P Request'] > .page-head > .container > .page-head-content > .page-title")
                console.log($("div[data-page-route='P2P Request'] > .page-head > .container > .page-head-content > .page-title"))
                console.log($("#loayalty_indicator"))
                $( "#loayalty_indicator" ).remove();
                // if(!tool_bar.find("#loayalty_indicator")){
                    tool_bar.append(btn)
                // }
                
                console.log($(".form-inner-toolbar"))
		}

	},
	workflow_state:function(frm){
		if (frm.doc.workflow_state == "P2P Issued" || frm.doc.workflow_state == "P2P Approved"){
			$("[data-label='Send To Unit']").parent().hide()
		}
	},
	before_save:async function(frm){
		if(!frm.doc.terms_check)
		{
			var terms_details = await frappe.db.get_value("Terms and Conditions","P2P Terms","terms")
			if(terms_details.message.terms){
				return new Promise(function(resolve, reject) {
					frappe.confirm(
						terms_details.message.terms,
						function() {
							var negative = 'frappe.validated = false';
							resolve(negative);
							frm.set_value("terms_check",1)
							frm.set_value("agree_to_terms_and_conditions",1)
							frm.refresh_field("terms_check","agree_to_terms_and_conditions")
						},
						function() {
							reject();
						}
					)
				})

			}
		}	

	},
	setup: function(frm) {
	    console.log(frappe.session.user)
		frm.set_query("distributor_name", function() {
			return {
				filters: {
				    "user_name":frappe.session.user
				},
				query:"replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.filter_distributor"
			}
		});
		frm.set_query("item_code", function() {
			return {
				filters: {
				    
				},
				query:"replacement_management.replacement_management.doctype.p2p_request.p2p_request.filter_item"
			}
		});
		// frm.set_query("replaced_item_code", function() {
		// 	return {
				
		// 		query:"replacement_management.replacement_management.doctype.regular_replacement_request.regular_replacement_request.get_rma_engineer"
		// 	}
		// });
	},
	validate(frm){
		// frappe.db.get_value("P2P Master",{"item_code":frm.doc.item_code},"status").then(function(res){
		// 	if(res.message){
		// 		if(res.message['status'] != "Enable"){
		// 		frappe.throw("Item is not available under P2P replacement, Please proceed with regular replacement")
		// 		}
		// 	}else{
		// 		frappe.throw("Item is not available under P2P replacement, Please proceed with regular replacement")
		// 	}
			
		// })
	},
	onload: function(frm) {
		if (frm.doc.workflow_state == "P2P Issued" || frm.doc.workflow_state == "P2P Approved"){
			$("[data-label='Send To Unit']").parent().hide()
		}
		// console.log(frappe.session.user)
		if(frm.is_new()){
			frappe.call({
				"method":"get_customer_details",
				"doc":frm.doc,
				"args":{"uname":frappe.session.user},
				"callback":function(res){
					console.log(res)
					if( res.message[0]){
						frm.set_value("customer_name",res.message[0])
						frm.refresh_field("customer_name")
					}
					if( res.message[1]){
						frm.set_value("customer_mobile_no",res.message[1])
						frm.refresh_field("customer_mobile_no")
					}
	
				}
	
			})

		}
		
		
	}
});

