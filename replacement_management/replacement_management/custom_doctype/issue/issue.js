frappe.ui.form.on('Issue', {
	async refresh(frm) {
		// buttons - code modified 21-09-2021 - Nikhil
		if (frm.doc.status !== "Closed") {
			if(frm.doc.status == "Recall Product") {
				frm.add_custom_button(__("Recall Product"), function() {
					frappe.model.open_mapped_doc({
						method: "replacement_management.replacement_management.custom_doctype.issue.issue.make_rr",
						frm: frm
					});
				});
			}
		}
	}
})