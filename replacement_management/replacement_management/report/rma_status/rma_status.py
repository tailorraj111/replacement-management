# Copyright (c) 2013, Raaj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data


def get_data(filters):
	data = frappe.db.sql("""
	select name as id_no,customer_name,workflow_state as status,item_code,total_amount,engineer_remark_for_customer, date as between_date,rma_engineer as engg_name,warranty_status,pendency_status,repair_status from `tabRegular Replacement Request` where date between %s and %s
	""",(filters.get("from_date"),filters.get("to_date")),as_dict = 1)
	return data

def get_columns(filters):
	columns = [
		{
			"label":_("Date"),
			"fieldname": "between_date",
			"fieldtype": "Date",
			"width": 100
		},
		{
			"label":_("ID No"),
			"fieldname": "id_no",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label":_("Status"),
			"fieldname": "status",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label":_("Customer Name"),
			"fieldname": "customer_name",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label":_("Engg Name"),
			"fieldname": "engg_name",
			"fieldtype": "Data",
			"width": 100
		},
		
		{
			"label":_("Item Code"),
			"fieldname": "item_code",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label":_("Warranty Status"),
			"fieldname": "warranty_status",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label":_("Repair Status"),
			"fieldname": "repair_status",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label":_("Pendency Status"),
			"fieldname": "pendency_status",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label":_("Total Amount"),
			"fieldname": "total_amount",
			"fieldtype": "Float",
			"width": 100
		},
		{
			"label":_("Engineer Remark"),
			"fieldname": "engineer_remark_for_customer",
			"fieldtype": "Data",
			"width": 100
		},


	]

	return columns