frappe.ui.form.on('Sales Order', {
    refresh :async function(frm, cdt, cdn) {
		// var me = this;
        if(frm.doc.docstatus == 1){
           
        }
        if (frm.doc.docstatus==0) {
			frm.add_custom_button(__('RMA'),
				function() {
					erpnext.utils.map_current_doc({
						method: "replacement_management.replacement_management.sales_order.make_sales_order_from_rma",
						source_doctype: "Regular Replacement Request",
						target: frm,
						setters: [
							{
								label: "Customer",
								fieldname: "customer_name",
								fieldtype: "Link",
								options: "Customer",
								default: frm.doc.customer || undefined
							}
						],
						get_query_filters: {
							workflow_state : "Ready to Dispatch Hold",
							approval_required : "Yes",
							total_amount:[">",0]
							
							// docstatus: 1,
							// workflow_state: ["=", "Dispatched"]
						}
					})
				}, __("Get Items From"));
		}


    }
})
